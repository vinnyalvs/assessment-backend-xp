<?php

use App\Controllers;

/**
 * Similar ao Laravel, aqui vamos definir as rotas, as respectivas classes
 * e os respectivos métodos que serão executados ao acessarem as rotas.
 * O PHP permite definir o classes e de métodos dinamicamente. Desta forma basta termos os nomes da classe e dos métodos como váriáveis.
 */
return [
    // Product Routes
    '/products' => [Controllers\ProductController::class, 'index'],
    '/addProduct' => [Controllers\ProductController::class, 'add'],
    '/storeProduct' => [Controllers\ProductController::class, 'store'],
    '/deleteProduct' => [Controllers\ProductController::class, 'delete'],
    '/editProduct' => [Controllers\ProductController::class, 'edit'],
    '/updateProduct' => [Controllers\ProductController::class, 'update'],
    // Category routes
    '/categories' => [Controllers\CategoryController::class, 'index'],
    '/storeCategory' => [Controllers\CategoryController::class, 'store'],
    '/addCategory' => [Controllers\CategoryController::class, 'add'],
    '/deleteCategory' => [Controllers\CategoryController::class, 'delete'],
    '/editCategory' => [Controllers\CategoryController::class, 'edit'],
    '/updateCategory' => [Controllers\CategoryController::class, 'update'],
    // Dashboard routes
    '/dashboard' => [Controllers\ProductController::class, 'dashboard'],
    '/' => [Controllers\ProductController::class, 'dashboard']
];

?>