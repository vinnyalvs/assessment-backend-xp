<?php

namespace App\Model;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;


/** @Entity */
class Product  implements \JsonSerializable
{
    /**
     * Product have many Groups.
     * @ManyToMany(targetEntity="Category",cascade={"persist"})
     * @JoinTable(name="product_categories",
     *      joinColumns={@JoinColumn(name="product_id", referencedColumnName="id")},
     *      inverseJoinColumns={@JoinColumn(name="category_id", referencedColumnName="id")}
     *
     *      )
     */
    private $categories;

    /**
     * Product constructor.
     * @param $categories
     * @param $id
     * @param $name
     * @param $SKU
     * @param $price
     * @param $quantity
     * @param $description
     */
    public function __construct()
    {
        $this->categories =  new ArrayCollection();
//        $this->id = $id;
//        $this->name = $name;
//        $this->SKU = $SKU;
//        $this->price = $price;
//        $this->quantity = $quantity;
//        $this->description = $description;
    }



    public function jsonSerialize(): array
    {
        return get_object_vars($this);
    }


    /**
     * @param  $category
     */
    public function addCategory($category)
    {
        if (!empty($categories) && $this->categories->contains($category)) {
            return;
        }

        $this->categories->add($category);
    }

    /**
     * @param Category $category
     */
    public function removeCategory(Category $category)
    {
        if (!$this->categories->contains($category)) {
            return;
        }

        $this->categories->removeElement($category);
        $category->removeProduct($this);
    }
    /**
     * @Id
     * @GeneratedValue
     * @Column(type="integer")
     */
    private $id;

    /**
     * @Column(type="string", length=255)
     */
    private $name;

    /**
     * @Column(type="string", length=255)
     */
    private $SKU;

    /**
     * @Column(type="decimal")
     */
    private $price;

    /**
     * @Column(type="integer")
     */
    private $quantity;

    /**
     * @Column(type="text")
     */
    private $description;



    public function getId()
    {
        return $this->id;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setPrice($price)
    {
        $this->price = $price;
    }

    public function getPrice()
    {
        return $this->price;
    }

    public function setDescription($description)
    {
        $this->description = $description;
    }


    public function getSKU()
    {
        return $this->SKU;
    }


    public function setSKU($SKU): void
    {
        $this->SKU = $SKU;
    }


    public function getQuantity()
    {
        return $this->quantity;
    }


    public function setQuantity($quantity): void
    {
        $this->quantity = $quantity;
    }


    public function getCategories()
    {
        return $this->categories;
    }


    public function setCategories($categories): void
    {
        $this->categories = $categories;
    }

    public function getDescription()
    {
        return $this->description;
    }

}

?>



