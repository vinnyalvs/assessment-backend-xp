<?php


namespace App\Controllers;


use App\data\EntityManagerFactory;
use App\Model\Category;
use App\Utils\LoggerHandler;
use App\Utils\ViewHandler;


/**
 * Class CategoryController
 * @package App\Controllers
 */
class CategoryController
{

    /**
     * @var \Doctrine\ORM\EntityManagerInterface
     */
    private $entityManager;
    /**
     * @var \Doctrine\Persistence\ObjectRepository
     */
    private $categoryRepository;
    /**
     * @var \Monolog\Logger
     */
    private $logger;

    /**
     * CategoryController constructor.
     * @throws \Doctrine\ORM\ORMException
     */
    public function __construct()
    {
        $this->logger = LoggerHandler::getLogger();
        $this->entityManager = EntityManagerFactory::getEntityManager();
        $this->categoryRepository = $this->entityManager->getRepository("App\Model\Category");

    }

    /**
     * Load Categories
     */
    public function index()
    {
        $this->logger->info("Loading categories");
        try {
            $data = [
                'categories' => $this->categoryRepository->findAll()
            ];
            ViewHandler::processView("category/categories", $data);
        } catch (\Error $e) {
            $this->logger->error("Error while loading categories");
            $this->logger->error($e);
        }

    }

    /**
     * Load addCategory
     */
    public function add()
    {
        try {
            ViewHandler::processView("category/addCategory", []);
        } catch (\Error $e) {
            $this->logger->error("Error while loading addCategory");
            $this->logger->error($e);
        }

    }

    /**
     * Persist a new category
     */
    public function store()
    {
        try {
            $post_vars = filter_input_array( INPUT_POST, FILTER_SANITIZE_STRING  ) ?? [];
            $category = new Category();
            $category->setCode($post_vars['code']);
            $category->setName($post_vars['name']);
            $this->entityManager->persist($category);
            $this->entityManager->flush();
            header('Location: /categories', false, 302);
        } catch (\Error $e) {
            $this->logger->error("Error while persisting category");
            $this->logger->error($e);
        }


    }

    /**
     * Delete a category
     */
    public function delete()
    {
        try {
            $vars = filter_input_array( INPUT_GET, FILTER_SANITIZE_STRING  ) ?? [];
            $categoryToRemove = $this->categoryRepository->find($vars['id']);
            $this->entityManager->remove($categoryToRemove);
            $this->entityManager->flush();
            header('Location: /categories', false, 302);
        } catch (\Error $e) {
            $this->logger->error("Error while deleting category");
            $this->logger->error($e);
        }


    }

    /**
     * Persist a category update after edit
     */
    public function update()
    {
        try {
            $post_vars = filter_input_array( INPUT_POST, FILTER_SANITIZE_STRING  ) ?? [];
            $categoryToUpdate = $this->categoryRepository->find($post_vars['id']);
            $categoryToUpdate->setCode($post_vars['code']);
            $categoryToUpdate->setName($post_vars['name']);
            $this->entityManager->persist($categoryToUpdate);
            $this->entityManager->flush();
            header('Location: /categories', false, 302);
        } catch (\Error $e) {
            $this->logger->error("Error while updating category");
            $this->logger->error($e);
        }

    }

    /**
     * Load editCategory
     */
    public function edit()
    {
        try {
            $vars = filter_input_array( INPUT_GET, FILTER_SANITIZE_STRING  ) ?? [];
            $categoryToEdit = $this->categoryRepository->find($vars['id']);
            $data = [
                'category' => $categoryToEdit
            ];
            ViewHandler::processView("category/editCategory",$data);
        } catch (\Error $e) {
            $this->logger->error("Error while loading editCategory");
            $this->logger->error($e);
        }

    }






}