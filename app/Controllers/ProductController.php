<?php


namespace App\Controllers;

use App\data\EntityManagerFactory;
use App\Model\Product;
use App\Utils\LoggerHandler;
use App\Utils\ViewHandler;


/**
 * Class ProductController
 * @package App\Controllers
 */
class ProductController
{
    /**
     * @var \Doctrine\ORM\EntityManagerInterface
     */
    private $entityManager;
    /**
     * @var \Doctrine\Persistence\ObjectRepository
     */
    private $productRepository;
    private $categoryRepository;
    private $logger;

    public function __construct()
    {
        $this->logger = LoggerHandler::getLogger();
        $this->entityManager = EntityManagerFactory::getEntityManager();
        $this->productRepository = $this->entityManager->getRepository("App\Model\Product");
        $this->categoryRepository = $this->entityManager->getRepository("App\Model\Category");
    }
    /**
     * load products
     */
    public function index()
    {
        $this->logger->info("Loading products");
        try {
            $products = $this->productRepository->findAll();
            $data = [
                'products' => $products
            ];
            ViewHandler::processView("product/products", $data);
        } catch (\Exception $e) {
            $this->logger->error("Error while Loading products");
            $this->logger->error($e);
        }

    }

    /**
     * Load addProduct
     */
    public function add()
    {
        $categories = $this->categoryRepository->findAll();
        try {
            $data = [
                'categories' => $categories
            ];
            ViewHandler::processView("product/addProduct", $data);
        } catch (\Exception $e) {
            $this->logger->error("Error while loading addProduct");
            $this->logger->error($e);
        }

    }

    /**
     * Load dashboard
     */

    public function dashboard()
    {
        try {
           $products = $this->productRepository->findAll();
            $data = [
                'products' => $products
            ];
            ViewHandler::processView("dashboard/dashboard",$data);
         }
         catch (\Exception $e) {
            $this->logger->error("Error while loading dashboard");
            $this->logger->error($e);
        }
    }

    /**
     * Persist a new product
     */
    public function store()
    {
        $post_vars = filter_input_array( INPUT_POST, FILTER_SANITIZE_STRING  ) ?? [];
        try {
            if(!is_null($post_vars)) {
                $product = new Product();
                $product->setName($post_vars['productName']);
                $product->setPrice($post_vars['price']);
                $product->setDescription($post_vars['description']);
                $product->setQuantity($post_vars['quantity']);
                $product->setSKU($post_vars['SKU']);

                foreach ($post_vars['categories'] as $categoryId) {
                    $category = $this->categoryRepository->find($categoryId);
                    $product->addCategory($category);
                }
                $this->entityManager->persist($product);
                $this->entityManager->flush();
                $this->logger->info("Loading products");
            }

            header('Location: /products', false, 302);
        } catch (\Exception $e) {
            $this->logger->error("Error while persisting product" );
            $this->logger->error($e);
        }


    }

    /**
     * Delete a product
     */

    public function delete()
    {
        try {
            $productToRemove = $this->productRepository->find($_GET['id']);
            $this->entityManager->remove($productToRemove);
            $this->entityManager->flush();
            header('Location: /products', false, 302);
        } catch (\Exception $e) {
            $this->logger->error("Error while deleting product");
            $this->logger->error($e);
        }

    }

    /**
     * Persist a product update after edit
     */
    public function update()
    {
        try {
            $post_vars = filter_input_array( INPUT_POST, FILTER_SANITIZE_STRING  ) ?? [];
            $entityManager = EntityManagerFactory::getEntityManager();
            $productRepository = $entityManager->getRepository("App\Model\Product");
            $productToUpdate= $productRepository->find($post_vars['id']);
            $productToUpdate->setName($post_vars['name']);
            $productToUpdate->setPrice($post_vars['price']);
            $productToUpdate->setDescription($post_vars['description']);
            $productToUpdate->setQuantity($post_vars['quantity']);
            $productToUpdate->setSKU($post_vars['SKU']);
            foreach ($post_vars['categories'] as $categoryId) {
                $category = $this->categoryRepository->find($categoryId);
                $productToUpdate->addCategory($category);
            }
            $entityManager->merge($productToUpdate);
            $entityManager->flush();

            header('Location: /products', false, 302);
        } catch (\Exception $e) {
            $this->logger->error("Error while updating product");
            $this->logger->error($e);
        }

    }

    /**
     * Load editProduct
     */
    public function edit()
    {
        try {
            $productToEdit = $this->productRepository->find($_GET['id']);
            $categories = $this->categoryRepository->findAll();
            $data = [
                'product' => $productToEdit,
                'categories' => $categories
            ];
            ViewHandler::processView("product/editProduct",$data);
        } catch (\Exception $e) {
            $this->logger->error("Error while loading editProduct page");
            $this->logger->error($e);
        }

    }




}