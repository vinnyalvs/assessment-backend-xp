# Webjump Backend Assessment

## Sobre Execução do projeto

## Requisitos para execução

* PHP 7.4
* sqlite
* Composer 2.0

## Bibliotecas utilizadas (php.ini)

* pdo_sqlite

## Como executar o servidor embutido do PHP

Para executar o projeto o servidor embutido do PHP ja é suficiente. Para rodar, na pasta raiz do projeto basta:

		php -S localhost:8080 -t public/

## Passo a passo execução

1. Clone o repositório através, ou faça download do zip e descompacte 
1. Habilite o pdo_sqlite no arquivo php.ini
1. Instale as dependências do composer usando composer install
1. Execute o servidor embutido do php -S localhost:8080 -t public/  
1. Acesse localhost:8080 com o navegador de preferência.

### Observação:
Também é possível rodar o projeto com algum servidor web mais robusto como o Apache ou NGINX. Dado que é um projeto simples para a avaliação, preferi executar com o servidor embutido e poupar esforço (meu e do avaliador)  de configurar o servidor web.
### Como verificar se esta faltando dependências?
* A biblioteca pdo_sqlite deve estar habilitada no arquivo php.ini. Para ver as bibliotecas que estão habilitadas basta usar
  php -m. Caso não esteja habilitado, basta ir na localização do arquivo php.ini para editar. Essa localização pode ser visualizada com o comando php --ini

### Pacotes do composer utilizados

* doctrine/ORM -> Pacote usado para criar o Objet Relational Mapper
* doctrine/cache -> Pacote necessário para que o Doctrine/ORM funcionasse
* symfony/cache -> Pacote necessário para que o Doctrine/ORM funcionasse
* doctrine/annotations -> Pacote que permite realizar as anotações nos atributos das Entidades
* monolog/monolog -> Pacote para criar logs de forma fácil

# Estrutura do Projeto

A ideia foi implementar uma versão simplificada de um framework MVC como o Laravel. Que permitisse simplificadamente criar as nossas entidades (model), controllers e views.

## Entidades

Para criar as entidades a ideia foi usar o Doctrine, que é um pacote ORM (object relational mapper), permitindo desenvolver toda a parte relacionada ao banco de dados de maneira fácil. Sem se preocupar com escrever SQls.

A tecnologia de banco de dados escolhida foi o SQLite, permitindo agilidade de instalação e configuração.

## Estrutura do banco de dados

Como descrito nas instruções do projeto, temos duas entidades, Product e Category. As duas entidades tem um relacionamento N para N, pois um produto pode ter várias categorias e naturalmente categorias tem mais de um produto. Desta forma, foi criada uma tabela para representar esse relacionamento. O Modelo pode ser observado com mais detalhes na imagem a seguir.

![DB](dbStructure.png)


### Estrutura do código

* Pasta **public** contém os assets de imagem e de estilo (css) e o arquivo index.php.
* Pasta **app/Controllers** contém os controladores responsáveis por intermediar as requisições.
* Pasta **app/data** contém o arquivo sqlite com o banco e a classe responsável pela conexão com o banco (EntityManager)
* Pasta **app/Model** contém as entidades da aplicação.
* Pasta **app/routes** contém um arquivo onde as rotas devem ser declaradas
* Pasta **app/Utils** contém a classe por processar e requisitar os arquivos de view
* Pasta **app/Views** contém as views da aplicação.

Os logs ficam na pasta raiz num arquivo chamado Log.log. 
### Comentários e evoluções

Algumas melhorias podem ser feitas para aperfeiçoar o projeto:

* Melhoria na validação dos formulários.
* Melhorar os logs, os logs ainda estão bem superficiais e não cobrem todas as ações.
* Melhorar os tratamentos de erro e exceções, que também estão com cobertura superficial.
* Criar testes de unidade
* Criar testes automatizados de interface com selenium
* Melhorar o sistema de renderização das views para evitar usar variáveis php. É possível criar um sistema parecidas com as blades do Laravel, por exemplo.
* A opção de delete na tela de produto e de categorias poderia ter um modal de confirmação.
* Adicionar opção de enviar imagem.

## Autor

Vinicius Alberto Alves da Silva

E-mail: viniciusaalberto@gmail.com

[LinkedIn](linkedin.com/in/vinicius-alves-9a45a3117)

[Github](https://github.com/vinnyalvs)
